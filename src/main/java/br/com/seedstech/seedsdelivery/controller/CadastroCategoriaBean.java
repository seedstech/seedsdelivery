package br.com.seedstech.seedsdelivery.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.seedstech.seedsdelivery.model.Categoria;
import br.com.seedstech.seedsdelivery.service.CategoriaService;
import br.com.seedstech.seedsdelivery.service.impl.NegocioException;
import br.com.seedstech.seedsdelivery.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroCategoriaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CategoriaService categoriaService;

	private Categoria categoria = new Categoria();

	public CadastroCategoriaBean() {
	}

	public void salvar() {
		try {
			categoriaService.salvar(categoria);
			FacesUtil.addInfoMessage("Categoria adicionada com sucesso!");
			limpar();

		} catch (NegocioException e) {
			FacesUtil.addErrorMessage(e.getMessage());
		}
	}

	public void limpar() {
		categoria = new Categoria();
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

}
