package br.com.seedstech.seedsdelivery.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import br.com.seedstech.seedsdelivery.model.Cliente;
import br.com.seedstech.seedsdelivery.model.Endereco;
import br.com.seedstech.seedsdelivery.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroClienteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	// @Inject
	// private ClienteService clienteService;

	private Cliente cliente;
	private String radioEndereco;

	public CadastroClienteBean() {
		cliente = new Cliente();

	}

	public void salvar() {
//		// clienteService.salvar(cliente);
//		limpar();
		FacesUtil.addInfoMessage("Cliente adicionado com sucesso!");
	}

	public void vincularEnderecos() {

		if (radioEndereco.equalsIgnoreCase("S")) {
			Endereco entrega = cliente.getMoradia();
			cliente.setEntrega(entrega);
		}
		if (radioEndereco.equalsIgnoreCase("N")) {
			
			cliente.setEntrega(new Endereco());
		}

	}

	private void limpar() {
		cliente = new Cliente();

	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getRadioEndereco() {
		return radioEndereco;
	}

	public void setRadioEndereco(String radioEndereco) {
		this.radioEndereco = radioEndereco;
	}

}
