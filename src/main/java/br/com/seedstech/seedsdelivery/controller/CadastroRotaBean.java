package br.com.seedstech.seedsdelivery.controller;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.seedstech.seedsdelivery.model.Bairro;
import br.com.seedstech.seedsdelivery.repository.BairroRepository;
import br.com.seedstech.seedsdelivery.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroRotaBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@Inject
	private BairroRepository bairroRepository;

	private Bairro bairro;

	public CadastroRotaBean() {
		bairro = new Bairro();
	}
	
	public void salvar(){
		bairroRepository.salvar(bairro);
		limpar();
		FacesUtil.addInfoMessage("Rota salva com sucesso!");
	}

	private void limpar() {
		bairro = new Bairro();
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}
}
