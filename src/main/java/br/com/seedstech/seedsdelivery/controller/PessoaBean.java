/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.seedstech.seedsdelivery.controller;

import br.com.seedstech.seedsdelivery.model.Contato;
import br.com.seedstech.seedsdelivery.repository.ContatoRepository;
import br.com.seedstech.seedsdelivery.service.PessoaServico;

import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class PessoaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    PessoaServico pessoaServico;
    
    
    @Inject
    private ContatoRepository contatoRepository;
    Pessoa pessoa;

    public Pessoa getPessoa() {
    	

        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
    
    public void exibirPessoa(){
    	Contato contato = new Contato();
    	contato.setCelular("889628939");
    	contato.setEmail("gadadw");
    	contato.setTelefone("wqwqw");
    	contatoRepository.salvar(contato);
        pessoa = pessoaServico.devolverPessoa();
    }
    
}
