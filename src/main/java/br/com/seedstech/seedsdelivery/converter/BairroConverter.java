package br.com.seedstech.seedsdelivery.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.seedstech.seedsdelivery.model.Bairro;
import br.com.seedstech.seedsdelivery.repository.impl.BairroRepositoryImpl;

@FacesConverter(value = "bairroConverter", forClass = Bairro.class)
public class BairroConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		Bairro retorno = null;

		if (value != null) {
			Integer id = new Integer(value);
			retorno = new BairroRepositoryImpl().porId(id);
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object value) {
		Bairro bairro = (Bairro) value;
		if (bairro == null || bairro.getId() == null) {
			return null;
		}
		return String.valueOf(bairro.getId());
	}
}
