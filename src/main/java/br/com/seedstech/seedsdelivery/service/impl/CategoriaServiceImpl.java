package br.com.seedstech.seedsdelivery.service.impl;

import javax.inject.Inject;

import br.com.seedstech.seedsdelivery.model.Categoria;
import br.com.seedstech.seedsdelivery.repository.CategoriaRepository;
import br.com.seedstech.seedsdelivery.service.CategoriaService;

public class CategoriaServiceImpl implements CategoriaService {

	private static final long serialVersionUID = 1L;

	@Inject
	private CategoriaRepository categoriaRepository;

	@Override
	public void salvar(Categoria categoria) {
		if (!nomeJaCadastrado(categoria)) {
			categoriaRepository.salvar(categoria);

		}

	}

	@Override
	public Boolean nomeJaCadastrado(Categoria categoria) {

		if (categoria.isEditando()) {
			return false;
		}

		if (categoriaRepository.existeCategoria(categoria)) {
			throw new NegocioException("Nome de categória já existente");
		}
		return false;
	}

}
