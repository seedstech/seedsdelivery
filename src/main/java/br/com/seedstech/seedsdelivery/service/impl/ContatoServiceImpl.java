package br.com.seedstech.seedsdelivery.service.impl;

import javax.inject.Inject;

import br.com.seedstech.seedsdelivery.model.Contato;
import br.com.seedstech.seedsdelivery.repository.ContatoRepository;
import br.com.seedstech.seedsdelivery.service.ContatoService;

public class ContatoServiceImpl implements ContatoService {

	private static final long serialVersionUID = 1L;

	@Inject
	private ContatoRepository contatoRepository;

	public void salvar(Contato contato) {
		contatoRepository.salvar(contato);
	}
}
