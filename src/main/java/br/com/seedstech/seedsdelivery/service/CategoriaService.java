package br.com.seedstech.seedsdelivery.service;

import java.io.Serializable;

import br.com.seedstech.seedsdelivery.model.Categoria;

public interface CategoriaService extends Serializable {

	public void salvar(Categoria categoria);

	public Boolean nomeJaCadastrado(Categoria nome);
}
