package br.com.seedstech.seedsdelivery.service;

import java.io.Serializable;
import java.util.List;

import br.com.seedstech.seedsdelivery.model.Cliente;

public interface ClienteService  extends Serializable{


	void salvar(Cliente cliente);

	void atualizar(Cliente cliente);

	void excluir(Cliente cliente);

	Cliente porId(Integer id);

	List<Cliente> porNome(String nome);

	List<Cliente> listar();
	
	Boolean enderecoResidenciaIgualEntrega(Cliente cliente);

}
