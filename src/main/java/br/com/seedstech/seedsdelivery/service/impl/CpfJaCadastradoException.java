package br.com.seedstech.seedsdelivery.service.impl;

public class CpfJaCadastradoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CpfJaCadastradoException(String msg) {
		super(msg);
	}
}
