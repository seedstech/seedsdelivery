//package br.com.seedstech.seedsdelivery.service.impl;
//
//import java.util.List;
//
//import javax.inject.Inject;
//
//import br.com.seedstech.seedsdelivery.model.Bairro;
//import br.com.seedstech.seedsdelivery.model.Cliente;
//import br.com.seedstech.seedsdelivery.model.Endereco;
//import br.com.seedstech.seedsdelivery.repository.BairroRepository;
//import br.com.seedstech.seedsdelivery.repository.ClienteRepository;
//
//public class ClienteServiceImpl  {
//
//
//	@Inject
//	private ClienteRepository clienteRepository;
//
//	@Inject
//	private BairroRepository bairroRepository;
//
////	@Override
//	public void salvar(Cliente entity) {
//
//		if (cpfJaCadastrado(entity.getCpf())) {
//			throw new CpfJaCadastradoException("O cpf informado já encontra-se na nossa base de dados");
//		}
//		/*
//		 * Verifica se foi informado no endereco de entrega o logradouro. Caso
//		 * tenha sido informado,deverá ser feito uma busca do banco de dados
//		 * para recuperar O bairro que pode ser gerenciado pelo Hibernate e seta
//		 * o relacionamento bi-direcional de cliente x endereço.
//		 */
//
//		if (!entity.getEntrega().getLogradouro().isEmpty() && entity.getEntrega().getLogradouro() != "") {
//			Integer bairroId = entity.getMoradia().getBairro().getId();
//			Bairro encontrado = bairroRepository.porId(bairroId);
//			entity.getMoradia().setBairro(encontrado);
//
//			entity.getMoradia().setCliente(entity);
//			entity.getEntrega().setBairro(encontrado);
//
//			clienteRepository.salvar(entity);
//
//		} else {
//			/*
//			 * Caso não tenha sido informado endereço de entrega, então deduz
//			 * que o endereço de moradia é o mesmo da entrega, logo é setado o
//			 * endereço de moradia dentro do endereço de entrega.
//			 * 
//			 */
//			Integer bairroId = entity.getMoradia().getBairro().getId();
//			Bairro encontrado = bairroRepository.porId(bairroId);
//			entity.getMoradia().setBairro(encontrado);
//
//			entity.getMoradia().setCliente(entity);
//			Endereco enderecoEntrega = entity.getMoradia();
//			enderecoEntrega.setBairro(encontrado);
//			entity.setEntrega(enderecoEntrega);
//
//			clienteRepository.salvar(entity);
//		}
//
//	}
//
//	public Cliente porId(Integer id) {
//		return clienteRepository.porId(id);
//	}
//
//	public List<Cliente> todos() {
//		return (List<Cliente>) clienteRepository.listar();
//	}
//
//	public void excluir(Integer id) {
////		clienteRepository.ex(id);
//	}
//
//	public void excluir(Cliente t) {
//		clienteRepository.excluir(t);
//	}
//
//	public Boolean existente(Integer id) {
////		return clienteRepository.(id);
//		return true;
//	}
//
//	public boolean cpfJaCadastrado(String cpf) {
//		Cliente cliente = clienteRepository.porCpf(cpf);
//		return cliente != null;
//	}
//
//}
