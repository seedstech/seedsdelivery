package br.com.seedstech.seedsdelivery.model;

/**
 * @author masyaf
 *
 */
public enum TipoAlimento {

	PIZZA("Pizza"),
	ESFIRRA("Esfirra");

	private String descricao;

	TipoAlimento(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
