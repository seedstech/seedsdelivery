
package br.com.seedstech.seedsdelivery.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author masyaf
 *
 */
@Entity
@Table(name = "item_pedido")
public class ItemPedido implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private Integer id;

	@Column(name = "quantidade", nullable = false, length = 3)
	private Integer quantidade = 1;

	@ManyToOne
	@JoinColumn(name = "produto_id", nullable = false)
	private Produto produto;
	
	@ManyToOne
	@JoinColumn(name = "pedido_id", nullable = false)
	private Pedido pedido;

	@Enumerated(EnumType.STRING)
	private TipoProduto tipoProduto;
	
	@Enumerated(EnumType.STRING)
	private TamanhoProduto tamanhoProdutoEscolhido;
	
	
	

	



	/**
	 * Método responsável por efetuar o calculo dos produtos, sejam eles Pizzas,
	 * esfirras, salgados, bebidas e entre outros. Para produtos do tipo Pizza,
	 * o método leva em consideração os Médio ou Grande para efetuar cálculo do
	 * valor total do pedido.
	 * 
	 * Para Produtos como esfirras, coxinhas, bebidas O método leva em
	 * consideração o valor unitário do mesmo.
	 * 
	 * Devido a estas restrições, faz-se necessário sempre verificar se os
	 * valores estão nullos pois dependendo da categoria do produto, o
	 * valorTamnhoMedio e valorTamanhoGrande poderão estar setados como null
	 * 
	 * @return
	 */
	@Transient
	public BigDecimal getValorTotal() {
		BigDecimal valorTotal = BigDecimal.ZERO;
		if (produto != null) {

			valorTotal = valorPeloTipoProduto(valorTotal);

		}

		return valorTotal;
	}

	/**
	 * @param valorTotal
	 * @return
	 */
	private BigDecimal valorPeloTipoProduto(BigDecimal valorTotal) {
		
		if (!isProdutoTamanhoDiferenciado()) {
			valorTotal = produto.getValorUnitario().multiply(new BigDecimal(this.quantidade));

		}

		if (isProdutoTamanhoDiferenciado()) {

			valorTotal = calculaValorProdutoInteira(valorTotal);

			valorTotal = calculaValorProdutoMeia(valorTotal);
		}
		return valorTotal;
	}

	/**
	 * @param valorTotal
	 * @return
	 */
	private BigDecimal calculaValorProdutoInteira(BigDecimal valorTotal) {
		if (TipoProduto.INTEIRA.equals(tipoProduto)) {

			if (TamanhoProduto.GRANDE.equals(tamanhoProdutoEscolhido)) {
				if (produto.getValorTamanhoGrande() != null) {
				
					valorTotal = produto.getValorTamanhoGrande().multiply(new BigDecimal(this.quantidade));
				}
			}
			if (TamanhoProduto.MEDIO.equals(tamanhoProdutoEscolhido)) {
				if (produto.getValorTamanhoMedio() != null) {
					
					valorTotal = produto.getValorTamanhoMedio().multiply(new BigDecimal(this.quantidade));

				}
			}
	
		}
		return valorTotal;
	}

	/**
	 * @param valorTotal
	 * @return
	 */
	private BigDecimal calculaValorProdutoMeia(BigDecimal valorTotal) {
		if (TipoProduto.MEIA.equals(tipoProduto)) {
			 
			 if (TamanhoProduto.GRANDE.equals(tamanhoProdutoEscolhido)) {
					BigDecimal valorMeiaTamanhoGrande = BigDecimal.ZERO; 
					valorMeiaTamanhoGrande = 
	                                       produto.getValorTamanhoGrande().
	                                                     divide(new BigDecimal("2.0"), 
	                  		                                              RoundingMode.CEILING);
					
			        valorTotal = valorTotal.add(valorMeiaTamanhoGrande);		
			}

			if (produto.getValorTamanhoMedio() != null) {
				BigDecimal valorMeiaTamanhoMedio = BigDecimal.ZERO; 
				valorMeiaTamanhoMedio = 
                                       produto.getValorTamanhoMedio().
                                                     divide(new BigDecimal("2.0"), 
                  		                                              RoundingMode.CEILING);
				
		        valorTotal = valorTotal.add(valorMeiaTamanhoMedio);

			}
		}
		return valorTotal;
	}

//	@Transient
//	public boolean isProdutoAssociado() {
//		return this.getProduto() != null && this.getProduto().getId() != null;
//	}




	private Boolean isProdutoTamanhoDiferenciado(){
		return produto.getValorUnitario() == null;
	}
	// @Transient
	// public boolean isEstoqueSuficiente() {
	// return this.getPedido().isEmitido() || this.getProduto().getId() == null
	// || this.getProduto().getQuantidadeEstoque() >= this.getQuantidade();
	// }


	// @Transient
	// public boolean isEstoqueInsuficiente() {
	// return !this.isEstoqueSuficiente();
	// }
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}

	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public TamanhoProduto getTamanhoProdutoEscolhido() {
		return tamanhoProdutoEscolhido;
	}

	public void setTamanhoProdutoEscolhido(TamanhoProduto tamanhoProdutoEscolhido) {
		this.tamanhoProdutoEscolhido = tamanhoProdutoEscolhido;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPedido other = (ItemPedido) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}