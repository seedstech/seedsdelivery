package br.com.seedstech.seedsdelivery.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "produto")
public class Produto implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotBlank(message = "É obrigatório informar o nome do produto")
	@Size(max = 80)
	@Column(nullable = false, length = 80)
	private String nome;

//	@NotNull(message = "E obrigatório informar o valor unitário")
	@Column(name = "valor_unitario", nullable = true, precision = 10, scale = 2)
	private BigDecimal valorUnitario;

//	@NotNull(message = "É obrigatório informar a quantidade em estoque")
//	@Min(0)
//	@Max(value = 9999, message = "Quantidade estoque tem um valor muito alto")
	@Column(name = "quantidade_estoque", nullable = true, length = 5)
	private Integer estoque;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "categoria_id", nullable = false)
	private Categoria categoria;

	@Max(value = 9999, message = "Quantidade valor tamanho Médio tem um valor muito alto")
	@Column(name = "valor_tamanho_medio", nullable = true, length = 5)
	private BigDecimal valorTamanhoMedio;

	@Max(value = 9999, message = "Quantidade valor tamanho Grande tem um valor muito alto")
	@Column(name = "valor_tamanho_grande", nullable = true, length = 5)
	private BigDecimal valorTamanhoGrande;

	/**
	 * Construtor utilizado para construção de produtos do tipo Bebidas
	 * 
	 * @param nome
	 * @param valorUnitario
	 * @param estoque
	 * @param categoria
	 */
	public Produto(String nome, BigDecimal valorUnitario, Integer estoque, Categoria categoria) {
		super();
		this.nome = nome;
		this.valorUnitario = valorUnitario;
		this.setEstoque(estoque);
		this.categoria = categoria;
	}

	/**
	 * Construtor utilizado para construção de produtos do tipo Pizzas
	 * 
	 * @param nome
	 * @param categoria
	 * @param valorTamanhoMedio
	 * @param valorTamanhoGrande
	 */

	public Produto(String nome, Categoria categoria, BigDecimal valorTamanhoMedio,
			BigDecimal valorTamanhoGrande) {
		super();
		this.nome = nome;
		this.categoria = categoria;
		this.valorTamanhoMedio = valorTamanhoMedio;
		this.valorTamanhoGrande = valorTamanhoGrande;
	}

	/**
	 * Construtor utilizado para construção de produtos do Tipo Salgados como
	 * Esfirras, coxinhas..
	 * 
	 * @param nome
	 * @param valorUnitario
	 * @param categoria
	 */

	public Produto(String nome, BigDecimal valorUnitario, Categoria categoria) {
		super();
		this.nome = nome;
		this.valorUnitario = valorUnitario;
		this.categoria = categoria;
	}

	public Produto() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}



	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public BigDecimal getValorTamanhoMedio() {
		return valorTamanhoMedio;
	}

	public void setValorTamanhoMedio(BigDecimal valorTamanhoMedio) {
		this.valorTamanhoMedio = valorTamanhoMedio;
	}

	public BigDecimal getValorTamanhoGrande() {
		return valorTamanhoGrande;
	}

	public void setValorTamanhoGrande(BigDecimal valorTamanhoGrande) {
		this.valorTamanhoGrande = valorTamanhoGrande;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * @return the estoque
	 */
	public Integer getEstoque() {
		return estoque;
	}

	/**
	 * @param estoque the estoque to set
	 */
	public void setEstoque(Integer estoque) {
		this.estoque = estoque;
	}

	// public void baixarEstoque(Integer quantidade) {
	// int novaQuantidade = this.getQuantidadeEstoque() - quantidade;
	//
	// if (novaQuantidade < 0) {
	// throw new NegocioException("Não há disponibilidade no estoque de " +
	// quantidade + " itens do produto.");
	// }
	//
	// this.setQuantidadeEstoque(novaQuantidade);
	// }
	//
	// public void adicionarEstoque(Integer quantidade) {
	// this.setQuantidadeEstoque(getQuantidadeEstoque() + quantidade);
	// }

}