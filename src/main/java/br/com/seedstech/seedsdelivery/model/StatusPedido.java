package br.com.seedstech.seedsdelivery.model;


public enum StatusPedido {

	INICIADO("Iniciado"), 
	PRONTO("Pronto"),
	SAIU_ENTREGA("Saiu para entrega"),
	RECEBIDO("Recebido"),
	RETORNO("Retorno");

	
	private String descricao;
	
	StatusPedido(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}