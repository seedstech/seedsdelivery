package br.com.seedstech.seedsdelivery.model;

/**
 * @author masyaf
 *
 */
public enum TamanhoProduto {

	MEDIO("Médio"),
	GRANDE("Grande");

	private String descricao;

	TamanhoProduto(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
