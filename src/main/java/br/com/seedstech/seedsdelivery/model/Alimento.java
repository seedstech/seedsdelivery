package br.com.seedstech.seedsdelivery.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 * The persistent class for the alimento database table.
 * 
 */
@Entity
@Table(name = "alimento")
public class Alimento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotBlank(message = "É obrigatório informar o nome do alimento")
	private String nome;

	@NotNull(message = "Valor é é obrigatório")
	@DecimalMin(value = "0.0", message = "O valor deve ser maior que R$0,50")
	@DecimalMax(value = "9999999.99", message = "O valor deve ser menor que R$9.999.999,99")
	private BigDecimal valor;

	private String recheio;

	@NotBlank(message = "É obrigatório informar uma descrição do alimento")
	private String descricao;

	public Alimento() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getRecheio() {
		return this.recheio;
	}

	public void setRecheio(String recheio) {
		this.recheio = recheio;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}