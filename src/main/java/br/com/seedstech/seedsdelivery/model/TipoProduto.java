package br.com.seedstech.seedsdelivery.model;

/**
 * @author masyaf
 *
 */
public enum TipoProduto {

	
	MEIA("Meia"),
	UNICO("Único"),
	INTEIRA("Inteira");
	
	private String descricao;

	TipoProduto(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
