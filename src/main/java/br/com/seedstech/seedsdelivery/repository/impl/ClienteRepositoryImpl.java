package br.com.seedstech.seedsdelivery.repository.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.seedstech.seedsdelivery.model.Cliente;
import br.com.seedstech.seedsdelivery.repository.ClienteRepository;
import br.com.seedstech.seedsdelivery.util.hibernate.HibernateUtil;

public class ClienteRepositoryImpl extends AbstractGenericRepository<Cliente> implements ClienteRepository {

	public ClienteRepositoryImpl() {
		super(Cliente.class);
	}

	private static final long serialVersionUID = 1L;

	@Override
	public Cliente porCpf(String cpf) {
		beginTransaction();
		Criteria criteria = HibernateUtil.getSession().createCriteria(Cliente.class);
		criteria.add(Restrictions.eq("cpf", cpf));
		Cliente toReturn = (Cliente) criteria.uniqueResult();
		HibernateUtil.closeSession();
		return toReturn;
	}

	@Override
	public Boolean existe(Cliente cliente) {
		beginTransaction();
		Criteria criteria = HibernateUtil.getSession().createCriteria(Cliente.class);
		criteria.add(Restrictions.eq("cpf", cliente.getCpf()));
		Boolean isExistente = criteria.list().isEmpty() ? false : true;
		HibernateUtil.closeSession();
		return isExistente;
	}

}
