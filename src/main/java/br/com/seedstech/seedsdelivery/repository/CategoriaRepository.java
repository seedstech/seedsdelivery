package br.com.seedstech.seedsdelivery.repository;

import br.com.seedstech.seedsdelivery.model.Categoria;

public interface CategoriaRepository extends IGenericRepository<Categoria> {

	Boolean  existeCategoria(Categoria categoria);

}
