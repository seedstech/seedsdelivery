package br.com.seedstech.seedsdelivery.repository;

import br.com.seedstech.seedsdelivery.model.Cliente;

public interface ClienteRepository extends IGenericRepository<Cliente> {

	Cliente porCpf(String cpf);
	
	Boolean existe(Cliente cliente);
	
	
	
}
