package br.com.seedstech.seedsdelivery.repository;

import java.io.Serializable;
import java.util.List;

public interface IGenericRepository<T>  extends Serializable{

	void salvar(T t);

	void atualizar(T t);

	void excluir(T t);

	T porId(Integer id);

	List<T> porNome(String nome);

	List<T> listar();

	void beginTransaction();

	void commitTransaction();

}
