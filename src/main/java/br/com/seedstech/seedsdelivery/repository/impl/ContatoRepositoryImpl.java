package br.com.seedstech.seedsdelivery.repository.impl;


import br.com.seedstech.seedsdelivery.model.Contato;
import br.com.seedstech.seedsdelivery.repository.ContatoRepository;

public class ContatoRepositoryImpl extends AbstractGenericRepository<Contato> implements ContatoRepository {

	private static final long serialVersionUID = 1L;

	public ContatoRepositoryImpl() {
		super(Contato.class);
	}


}
