package br.com.seedstech.seedsdelivery.repository;

import br.com.seedstech.seedsdelivery.model.Bairro;

public interface BairroRepository extends IGenericRepository<Bairro>{

}
