package br.com.seedstech.seedsdelivery.repository;

import br.com.seedstech.seedsdelivery.model.Contato;

public interface ContatoRepository extends IGenericRepository<Contato> {

}
