package br.com.seedstech.seedsdelivery.repository.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import br.com.seedstech.seedsdelivery.repository.IGenericRepository;
import br.com.seedstech.seedsdelivery.util.hibernate.HibernateUtil;


/*
 * @author masyaf
 * Essa classe é responsável por realizar as operações básicas no banco de dados com
 * salvar, atualizar, excluir e listar.
 *
 */

public abstract class AbstractGenericRepository<T> implements IGenericRepository<T>, Serializable {

	private static final long serialVersionUID = 1017454431151066494L;

	private Class<T> persistentClass;
	// private final Logger logger =
	// Logger.getLogger(persistentClass.getDeclaringClass().getName());

	@SuppressWarnings("unchecked")
	public AbstractGenericRepository(@SuppressWarnings("rawtypes") Class persistentClass) {
		super();
		this.persistentClass = persistentClass;
	}

	/**
	 * Método responsável por efetuar uma operação para salvar uma entidade T
	 * 
	 * @param T
	 */

	@Override
	public void salvar(T entity) {
		try {
			// logger.info("Iniciando a transação para salvar a classe " +
			// entity.getClass().getName());
			beginTransaction();
//			HibernateUtil.getSession().flush();
//			HibernateUtil.getSession().clear();
			// logger.info("Começandoo processo de salvar a entidade na sessão
			// aberta");
			HibernateUtil.getSession().saveOrUpdate(entity);
			// logger.info("Realizando o commit para efetuar alterações no banco
			// para a classe " + entity.getClass().getName());
			HibernateUtil.commitTransaction();
			// logger.info("Alterações realizadas com sucesso no banco de
			// dados!");

		} catch (HibernateException e) {

			// logger.info("Aconteceu alguma exceção... Desfazendo as alterações
			// no banco de dados!");
			HibernateUtil.rollBackTransaction();
			System.out.println("Não foi possível inserir " + entity.getClass() + ". Erro: " + e.getMessage());
			e.printStackTrace();
		} finally {
			HibernateUtil.closeSession();
			// logger.info("Finalizando a sessão ativa para a classe " +
			// entity.getClass().getName());
		}

	}

	/**
	 * Método responsável por realizar excluir um Objeto
	 * 
	 * @param T
	 */
	@Override
	public void excluir(T entity) {
		try {
			beginTransaction();
			HibernateUtil.getSession().delete(entity);
			commitTransaction();
		} catch (HibernateException e) {
			e.printStackTrace();
			HibernateUtil.rollBackTransaction();
		} finally {
			HibernateUtil.closeSession();
		}
	}

	/**
	 * Método responsável por realizar uma busca no banco de dados através do
	 * paramatro passado. Como retorno, obtém a entidade que corrende ao id
	 * 
	 * @param Integer
	 * @return T
	 */
	@Override
	public T porId(Integer id) {
		try {
			beginTransaction();
			Criteria criteria = HibernateUtil.getSession().createCriteria(persistentClass);
			@SuppressWarnings("unchecked")
			T t = (T) criteria.add(Restrictions.eq("id", id)).uniqueResult();
			return t;
		} catch (HibernateException e) {
			System.out.println("Não foi possível localizar por id o objeto"  +persistentClass.getName() +
					" Causa: " +e.getCause());
			HibernateUtil.rollBackTransaction();
		} finally {
			HibernateUtil.closeSession();
		}
		return null;

	}

	/**
	 * Método responsável por realizar uma busca no banco de dados através do
	 * paramatro passado. Como retorno, obtém a entidade que corrende ao nome
	 * 
	 * @param String
	 * @return {@link List}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> porNome(String nome) {
		List<T> toReturn = null;
		try {
			beginTransaction();
			Criteria criteria = HibernateUtil.getSession().createCriteria(persistentClass);
			return toReturn = criteria.add(Restrictions.eq("nome", nome)).list();

		} catch (HibernateException e) {
			System.out.println("Não foi possível localizar por nome" + e.getCause());
			HibernateUtil.rollBackTransaction();
		} finally {
			HibernateUtil.closeSession();
		}
		return toReturn;

	}

	/**
	 * Método responsável por obter todos os registros e e retornar uma lista de
	 * T
	 * 
	 * @return {@link List}
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> listar() {
		List<T> t = null;
		try {
			beginTransaction();
			Criteria criteria = HibernateUtil.getSession().createCriteria(persistentClass);
			return t = criteria.list();
		} catch (HibernateException e) {
			System.out.println("Não foi possível listar " + e.getMessage());
			HibernateUtil.rollBackTransaction();
		} finally {
			HibernateUtil.closeSession();
		}
		return t;
	}

	/**
	 * Método responsável por atualizar uma entidade
	 * 
	 * @param T
	 * 
	 */
	public void atualizar(T entity) {
		try {
			beginTransaction();
			HibernateUtil.getSession().update(entity);
			commitTransaction();
		} catch (HibernateException e) {
			System.out.println("Não foi possível atualizar " + e.getMessage());
		} finally {
			// HibernateUtil.closeSession();
			closeSession();
		}

	}

	/*
	 * Inicia uma transação através da classe HibernateUtil
	 * 
	 * 
	 */
	@Override
	public void beginTransaction() {
		HibernateUtil.beginTransaction();
	}

	/*
	 * Realiza o commit na transação
	 * 
	 * 
	 */

	@Override
	public void commitTransaction() {
		HibernateUtil.commitTransaction();
	}

	public void closeSession() {
		HibernateUtil.closeSession();
	}

}