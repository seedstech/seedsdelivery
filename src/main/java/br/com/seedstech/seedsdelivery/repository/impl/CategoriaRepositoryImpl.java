package br.com.seedstech.seedsdelivery.repository.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.seedstech.seedsdelivery.model.Categoria;
import br.com.seedstech.seedsdelivery.repository.CategoriaRepository;
import br.com.seedstech.seedsdelivery.util.hibernate.HibernateUtil;

public class CategoriaRepositoryImpl extends AbstractGenericRepository<Categoria> implements CategoriaRepository {

	private static final long serialVersionUID = 1L;

	public CategoriaRepositoryImpl() {
		super(Categoria.class);
	}

	@Override
	public Boolean existeCategoria(Categoria categoria) {
		beginTransaction();
		Criteria criteria = HibernateUtil.getSession().createCriteria(Categoria.class);
		criteria.add(Restrictions.eq("nome", categoria.getNome()).ignoreCase());

		Boolean existeCategoria = criteria.list().size() > 0;
		HibernateUtil.closeSession();
		return existeCategoria;
	}

}
