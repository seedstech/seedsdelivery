package br.com.seedstech.seedsdelivery.repository.impl;

import br.com.seedstech.seedsdelivery.model.Bairro;
import br.com.seedstech.seedsdelivery.repository.BairroRepository;

public class BairroRepositoryImpl extends AbstractGenericRepository<Bairro> implements BairroRepository {
	private static final long serialVersionUID = 1L;

	public BairroRepositoryImpl() {
		super(Bairro.class);
	}


}
