
# Informações Técnicas #

O sistema esta sendo desenvolvido utilizando uma plataforma baseada na tecnologia Java EE e seus frameworks. Uma vez que possui um ambiante de simples estruturação e de facil manutenção.
Dentre as tecnologias adotadas, listo as seguintes:

* Primefaces 6.0
* CDI (Weld) 2.1
* Hibernate 4.3 
* PostgreSQL 9.1
* JUNIT 4.12
* CDI Unit 3.1.3
* Maven
* Tomcat 8

